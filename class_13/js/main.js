jQuery(document).ready(function($) {

    var pageSlides = $(".homepage-slides");

    pageSlides.owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        nav: true,
        dots: true,
        navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });

    pageSlides.on('translate.owl.carousel', function(event) {

        $('.homepage-slides h4').removeClass('animated fadeInDownBig');
        $('.homepage-slides h1').removeClass('animated bounceInRight');
        $('.homepage-slides p').removeClass('animated fadeIn');

    });

    pageSlides.on('translated.owl.carousel', function(event) {

        $('.homepage-slides h4').addClass('animated fadeInDownBig');
        $('.homepage-slides h1').addClass('animated bounceInRight');
        $('.homepage-slides p').addClass('animated fadeIn');

    });

    $('.welcome-area').YTPlayer({
        fitToBackground: true,
        videoId: 'LSmgKRx5pBo'
    });

    //$(".video-play-btn").modalVideo();

});