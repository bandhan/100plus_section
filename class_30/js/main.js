(function($) {

    if (!$) {
        return;
    }

    ////////////
    // Plugin //
    ////////////

    $.fn.headroom = function(option) {
        return this.each(function() {
            var $this = $(this),
                data = $this.data('headroom'),
                options = typeof option === 'object' && option;

            options = $.extend(true, {}, Headroom.options, options);

            if (!data) {
                data = new Headroom(this, options);
                data.init();
                $this.data('headroom', data);
            }
            if (typeof option === 'string') {
                data[option]();

                if (option === 'destroy') {
                    $this.removeData('headroom');
                }
            }
        });
    };

    //////////////
    // Data API //
    //////////////

    $('[data-headroom]').each(function() {
        var $this = $(this);
        $this.headroom($this.data());
    });

}(window.Zepto || window.jQuery));

jQuery(document).ready(function($) {

    $('.portfolio-list').masonry({
        columnWidth: 1,
        percentPosition: true
    });

    $('.single-portfolio-item').hover(function() {
        $('.single-portfolio-item .portfolio-hover h2').removeClass('animated slideInDown');
        $(this).find('.portfolio-hover h2').addClass('animated slideInDown');
    });

    $(".menu-trigger").on('click', function() {
        $('.offcanvas-menu').addClass('active');
        $('.offcanvas-menu-overlay').addClass('active');
    });
    $(".menu-close i.fa,.offcanvas-menu-overlay").on('click', function() {
        $('.offcanvas-menu').removeClass('active');
        $('.offcanvas-menu-overlay').removeClass('active');
    });

    $('.header-area').headroom();

});