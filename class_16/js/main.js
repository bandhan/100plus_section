jQuery(document).ready(function($) {

    $('body').perfectScrollbar();

    $('.scrolled-paragraph').perfectScrollbar();

    $('.portfolio-filter li').on('click', function() {
        $('.portfolio-filter li').removeClass('active');
        $(this).addClass('active');
    });

    $('.portfolio-list').filterizr({
        layout: 'sameWidth'
    });

});