jQuery(document).ready(function($) {
    // $('.portfolio-list').masonry({
    //     horizontalOrder: true
    // });

    $('.portfolio-filter li').on('click', function() {
        $('.portfolio-filter li').removeClass('active');
        $(this).addClass('active');
    });

    $('.portfolio-list').filterizr({
        layout: 'sameWidth'
    });

});